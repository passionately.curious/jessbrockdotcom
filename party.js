document.addEventListener('DOMContentLoaded', function () {
  var partyModeSwitch = document.getElementById('partyMode');
  var headerElement = document.querySelector('header');
  var imageElements = document.querySelectorAll('img');
  toggleDarkMode(partyModeSwitch.checked);

  partyModeSwitch.addEventListener('change', function() {
      toggleDarkMode(this.checked);
  });

  function toggleDarkMode(isDarkMode) {
      if (isDarkMode) {
          document.body.classList.add('dark-mode'); 
          headerElement.classList.add('dark-mode-header'); 
          imageElements.forEach(img => img.classList.add('dark-mode-img'));
      } else {
          document.body.classList.remove('dark-mode');
          headerElement.classList.remove('dark-mode-header');
          imageElements.forEach(img => img.classList.remove('dark-mode-img'));
      }
  }
});